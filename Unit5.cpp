#include <exception>
#include <iostream>
#include <stdexcept>

class Fraction
{
public:
	Fraction(const float& Numerator, const float& Denominator)
	{
		this->Numerator = Numerator;
		this->Denominator = Denominator;
	}
	float Frac()
	{
		return Numerator / Denominator;
	}

private:
	float Numerator;
	float Denominator;
};

int main()
{
	std::cout << "Enter two numbers: " << '\n';
	try
	{
		float a = 0;
		if (!(std::cin >> a)) {
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			throw std::runtime_error("Numerator must be a number");
		}
		float b = 0;
		if (!(std::cin >> b)) {
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			throw std::runtime_error("Denominator must be a number");
		}
		if (b == 0)
		{
			throw std::runtime_error("Denominator must not be equal 0");
		}
		Fraction fraction(a, b);
		std::cout << "a / b = " << fraction.Frac() << '\n';
		return 0;
	}
	catch (const std::exception& err)
	{
		std::cout << "Thrown exception " << err.what() << '\n';
		return 1;
	}
}